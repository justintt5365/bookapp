from importlib.resources import path
from django.urls import path
from .views import books_list, create_view, show_book, update_book, delete_book, review_list

urlpatterns = [
    path('', books_list, name='books_list'),
    path('reviews/', review_list, name='review_list'),
    path('create/', create_view, name='create_view'),
    path('<int:pk>/', show_book, name="show_book"),
    path('<int:pk>/update/', update_book, name="update_book"),
    path('<int:pk>/delete/', delete_book, name="delete_book")
]
