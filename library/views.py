from django.shortcuts import render, redirect, HttpResponseRedirect, get_object_or_404
from pkg_resources import require

from .forms import BookForm, MagazineForm
from .models import Book, Magazine, Genre
from django.contrib.auth.decorators import login_required

# Create your views here.
def books_list(request):
    list_of_books = Book.objects.all()
    context = {
        "books" : list_of_books,
    }

    return render(request, "books/book_list.html", context)

# Login required decorator -- only lets following view by used when someone is logged in
@login_required
def review_list(request):
    book_reviews = request.user.reviews.all()
    context = {
        'reviews' : book_reviews
    }
    return render(request, "books/review_list.html", context)

def create_view(request):
    form = BookForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("books_list")
    context = {}
    context['form'] = form
    return render(request, "books/create_view.html", context)


def show_book(request, pk):
    book = Book.objects.get(pk=pk)
    context = {
        "book": book
    }
    return render(request, 'books/detail.html', context)

def update_book(request, pk):
    book = Book.objects.get(pk=pk)

    form = BookForm(request.POST or None, instance=book)
    if form.is_valid():
        form.save()
        return redirect("books_list")

    context = {
        "book": book
    }
    context['form'] = form
    return render(request, "books/update_book.html", context)

def delete_book(request, pk):
    book = Book.objects.get(pk=pk)

    if request.method =="POST":
        book.delete()
        return redirect('books_list')
        
    context = {
        "book": book
    }
    return render(request, "books/delete_book.html", context)

@login_required
def magazine_list(request):
    list_of_magazines = request.user.magazines.all()
    context = {
        "magazines" : list_of_magazines,
    }

    return render(request, "books/magazine_list.html", context)

def magazine_create_view(request):
    form = MagazineForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("magazine_list")
    context = {}
    context['form'] = form
    return render(request, "books/magazine_create_view.html", context)

def show_magazine(request, pk):
    magazine = Magazine.objects.get(pk=pk)
    context = {
        "magazine": magazine
    }
    return render(request, 'books/magazine_detail.html', context) 
    
def update_magazine(request, pk):
    magazine = Magazine.objects.get(pk=pk)

    form = MagazineForm(request.POST or None, instance=magazine)
    if form.is_valid():
        form.save()
        return redirect("magazine_list")

    context = {
        "magazine": magazine
    }
    context['form'] = form
    return render(request, "books/update_magazine.html", context)

def delete_magazine(request, pk):
    magazine = Magazine.objects.get(pk=pk)

    if request.method =="POST":
        magazine.delete()
        return redirect('magazine_list')
        
    context = {
        "magazine": magazine
    }
    return render(request, "books/delete_magazine.html", context)

def magazine_genres(request, pk):
    genre = Genre.objects.get(pk=pk)
    list_of_magazines = Magazine.objects.all()
    context = {
        "genre": genre,
        'magazines': list_of_magazines
    }

    return render(request, "books/magazine_genre.html", context)

