# Generated by Django 4.0.6 on 2022-07-20 22:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('library', '0005_magazine'),
    ]

    operations = [
        migrations.AlterField(
            model_name='magazine',
            name='description',
            field=models.TextField(max_length=5000, null=True),
        ),
        migrations.CreateModel(
            name='BookReview',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField()),
                ('book', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='reviews', to='library.book')),
            ],
        ),
    ]
