# Generated by Django 4.0.6 on 2022-07-20 18:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('library', '0004_alter_book_publish_date'),
    ]

    operations = [
        migrations.CreateModel(
            name='Magazine',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=125, unique=True)),
                ('release_cycle', models.CharField(max_length=125)),
                ('description', models.TextField(max_length=5000)),
                ('cover_image', models.URLField(blank=True, null=True)),
            ],
        ),
    ]
