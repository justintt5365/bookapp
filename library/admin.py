from django.contrib import admin
from .models import Book, Magazine, BookReview, Author, Genre, Issue

# Register your models here.

admin.site.register(Book)
admin.site.register(Magazine)
admin.site.register(BookReview)
admin.site.register(Author)
admin.site.register(Genre)
admin.site.register(Issue)

