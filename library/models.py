from enum import unique
from unicodedata import name
from django.db import models
from django.contrib.auth.models import User

# Create your models here.



class Book(models.Model):
    name = models.CharField(max_length=125, unique=True)
    authors = models.ManyToManyField("Author", related_name="books")
    number_of_pages = models.SmallIntegerField(null=True)
    bookcover = models.URLField(null=True, blank=True)
    isbn = models.BigIntegerField(null=True)
    in_print = models.BooleanField(null=True)
    publish_date = models.DateField(null=True)

    def __str__(self):
        return self.name + " by " + str(self.authors.first())

class Author(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name 


class BookReview(models.Model):
    reviewer = models.ForeignKey(User, related_name='reviews', on_delete=models.CASCADE)
    book = models.ForeignKey(Book, related_name='reviews', on_delete=models.CASCADE)
    text = models.TextField()

class Magazine(models.Model):
    title = models.CharField(max_length=125, unique=True)
    release_cycle = models.CharField(max_length=125)
    description = models.TextField(max_length=5000, null=True)
    cover_image = models.URLField(null=True, blank=True)
    genres = models.ManyToManyField("Genre", related_name='magazines')
    creator = models.ForeignKey(User, related_name='magazines', on_delete=models.CASCADE)

    def __str__(self):
        return self.title 

class Genre(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name 

class Issue(models.Model):
    issue_number = models.SmallIntegerField(null=True)
    cover_image = models.URLField(null=True, blank=True)
    date_published = models.DateField(null=True)
    page_count = models.SmallIntegerField(null=True)
    title = models.CharField(max_length=125, unique=True)
    description = models.TextField(max_length=5000, null=True)
    magazine = models.ForeignKey(Magazine, related_name='issues', on_delete=models.CASCADE)
    