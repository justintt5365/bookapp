from django import forms
from .models import Book, Magazine


class BookForm(forms.ModelForm):
    class Meta:
        model = Book    

        fields = [
            'name',
            'authors',
            'number_of_pages',
            'bookcover',
            'isbn',
            'in_print',
            'publish_date',
        ]

class MagazineForm(forms.ModelForm):
    class Meta:
        model = Magazine    

        fields = [
            'title',
            'release_cycle',
            'description',
            'cover_image',
            'genres'
        ]