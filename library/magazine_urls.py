from importlib.resources import path
from django.urls import path
from .views import magazine_list, magazine_create_view, show_magazine, update_magazine, delete_magazine, magazine_genres

urlpatterns = [
    path('', magazine_list, name='magazine_list'),
    path('create/', magazine_create_view, name='magazine_create_view'),
    path('<int:pk>/', show_magazine, name="show_magazine"),
    path('<int:pk>/update/', update_magazine, name="update_magazine"),
    path('<int:pk>/delete/', delete_magazine, name="delete_magazine"),
    path('genres/<int:pk>/', magazine_genres, name="magazine_genres"),
]
